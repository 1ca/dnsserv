# DNS Server

> MongoDB-backed DNS Server

### Details

**Supported Record Types**:
+ A
+ CNAME
+ TXT

**Example MongoDB Record**:
```json
{
  "url": "example.com.",
  "type": "cname",
  "val": "google.com."
}

{
  "url": "example.com.",
  "type": "a",
  "val": "8.8.8.8"
}
```
