package main

import (
	"net"

	"github.com/miekg/dns"
)

type Record struct {
	URL  string `json:"url" bson:"url"`
	Type string `json:"type" bson:"type"`
	Val  string `json:"val" bson:"val"`
}

type handler struct {
	readprimary bool
	ttl         uint32

	primary   map[string]map[uint16]string
	secondary map[string]map[uint16]string
}

func newHandler(ttl uint32) handler {
	return handler{
		readprimary: true,
		ttl:         ttl,
		primary:     make(map[string]map[uint16]string),
		secondary:   make(map[string]map[uint16]string),
	}
}

func (h *handler) Get(url string, rtype uint16) string {
	if h.readprimary {
		return h.primary[url][rtype]
	}

	return h.secondary[url][rtype]
}

func (h *handler) Set(url, val string, rtype uint16) {
	if !h.readprimary {
		if _, ok := h.primary[url]; ok {
			h.primary[url][rtype] = val
			return
		}

		h.primary[url] = map[uint16]string{rtype: val}
		return
	}

	if _, ok := h.secondary[url]; ok {
		h.secondary[url][rtype] = val
		return
	}

	h.secondary[url] = map[uint16]string{rtype: val}
}

func (h *handler) SwapCache() {
	h.readprimary = !h.readprimary
	if h.readprimary {
		h.secondary = make(map[string]map[uint16]string)
		return
	}

	h.primary = make(map[string]map[uint16]string)
}

func (h *handler) ServeDNS(res dns.ResponseWriter, req *dns.Msg) {
	msg := dns.Msg{}
	msg.SetReply(req)

	msg.Authoritative = true
	domain := msg.Question[0].Name

	val := h.Get(domain, req.Question[0].Qtype)
	if val != "" {
		switch req.Question[0].Qtype {
		case dns.TypeA:
			msg.Answer = append(msg.Answer, &dns.A{
				Hdr: dns.RR_Header{Name: domain, Rrtype: dns.TypeA, Class: dns.ClassINET, Ttl: h.ttl},
				A:   net.ParseIP(val),
			})
			break
		case dns.TypeCNAME:
			msg.Answer = append(msg.Answer, &dns.CNAME{
				Hdr:    dns.RR_Header{Name: domain, Rrtype: dns.TypeCNAME, Class: dns.ClassINET, Ttl: h.ttl},
				Target: val,
			})
			break
		case dns.TypeTXT:
			msg.Answer = append(msg.Answer, &dns.TXT{
				Hdr: dns.RR_Header{Name: domain, Rrtype: dns.TypeTXT, Class: dns.ClassINET, Ttl: h.ttl},
				Txt: []string{val},
			})
			break
		}
	}

	res.WriteMsg(&msg)
}
