FROM scratch
MAINTAINER Seth Moeckel <seth@eistudios.org>

EXPOSE 5000
ENV DNSSERV_LISTEN_ADDR ":5000"
ENV DNSSERV_MONGO_DB_URL "mongodb://mongo:27017"
ENV DNSSERV_SYNC_INTERVAL "30"

COPY /bin/dnsserv /
CMD ["/dnsserv"]
