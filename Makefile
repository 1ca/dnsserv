build:
	-rm -r bin
	-mkdir -p bin
	CGO_ENABLED=0 GOOS=linux go build -o bin/dnsserv handler.go main.go

run:
	DNSSERV_LISTEN_ADDR=":5000" DNSSERV_MONGO_DB_URL="mongodb://localhost:27017" DNSSERV_SYNC_INTERVAL=30 go run handler.go main.go
