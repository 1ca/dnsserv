package main

import (
	"fmt"
	"os"
	"time"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/kelseyhightower/envconfig"
	"github.com/miekg/dns"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
)

type Config struct {
	ListenAddr   string `required:"true" split_words:"true"`
	MongoDbUrl   string `required:"true" split_words:"true"`
	SyncInterval int64  `required:"true" split_words:"true"`
	DnsTTL       uint32 `default:"1" split_words:"true"`
	DevLogMode   bool   `default:"true" split_words:"true"`
}

func main() {
	conf := Config{}
	err := envconfig.Process("dnsserv", &conf)
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}

	level := zerolog.InfoLevel
	if conf.DevLogMode {
		level = zerolog.DebugLevel
	}

	logger := log.Output(zerolog.ConsoleWriter{Out: os.Stderr}).Level(level)
	session, err := mgo.Dial(conf.MongoDbUrl)
	if err != nil {
		logger.Fatal().Err(err).Str("url", conf.MongoDbUrl).Msg("failed to dial mongodb")
	}

	h := newHandler(conf.DnsTTL)
	go func() {
		ticker := time.NewTicker(time.Duration(int64(time.Second) * conf.SyncInterval))
		types := map[string]uint16{
			"a":     dns.TypeA,
			"txt":   dns.TypeTXT,
			"cname": dns.TypeCNAME,
		}

		for {
			records := []Record{}
			err := session.DB("1ca").C("dns").Find(bson.M{}).All(&records)
			if err != nil {
				logger.Error().Err(err).Msg("failed to retrieve records from mongodb")
			} else {
				for _, record := range records {
					h.Set(record.URL, record.Val, types[record.Type])
				}

				h.SwapCache()
			}

			// wait to loop again
			logger.Debug().Str("time", time.Now().UTC().String()).Msg("updated dns cache")
			<-ticker.C
		}
	}()

	srv := &dns.Server{Addr: conf.ListenAddr, Net: "udp", Handler: &h}
	logger.Info().Str("addr", srv.Addr).Msg("starting dns listener")

	if err := srv.ListenAndServe(); err != nil {
		logger.Fatal().Err(err).Msg("failed to start dns listener")
	}
}
